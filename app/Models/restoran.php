<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class restoran extends Model
{
    use HasFactory;
    protected $table = 'restoran';
    protected $fillable = ['nama', 'desc', 'harga'];
}
